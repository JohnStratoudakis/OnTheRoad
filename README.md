# OnTheRoad
OnTheRoad

[![Build Status](https://travis-ci.org/JohnStratoudakis/OnTheRoad.svg?branch=master)](https://travis-ci.org/JohnStratoudakis/OnTheRoad)

# Fedora Core 27
I had to install pip3.7 by running:

```wget https://bootstrap.pypa.io/get-pip.py```

and then installing it via:

```sudo python3.7 ./get-pip.py```
