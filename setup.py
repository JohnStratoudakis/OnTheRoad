#!/usr/bin/python3.7

import versioneer
from setuptools import setup
  
setup(
    name="OnTheRoad",
#    version="0.0.3",
    cmdclass=versioneer.get_cmdclass(),
    version=versioneer.get_version(),
    author="John Stratoudakis",
    author_email="johnstratoudakis@gmail.com",
    license="",
    packages=["OnTheRoad"],
    entry_points={
        "console_scripts": [
            "onTheRoad=OnTheRoad.travel_main:main"
            ]
        },
)
# setup( version=versioneer.get_version(),
#                 cmdclass=versioneer.get_cmdclass(),  ...)
